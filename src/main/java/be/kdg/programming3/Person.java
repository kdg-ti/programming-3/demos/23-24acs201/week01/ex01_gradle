package be.kdg.programming3;

import com.afrunt.randomjoke.Joke;
import com.afrunt.randomjoke.Jokes;

import java.util.Optional;

public class Person {

	private Jokes jokes;
	public Person() {
		 jokes = new Jokes()
			.withDefaultSuppliers();
	}

	public void tellJoke() {
		Optional<Joke> joke = jokes.randomJoke();
		if (joke.isPresent()) {
			System.out.println(joke.get().getText());
		} else {
			System.out.println("Not Funny");
		}
	}
}
