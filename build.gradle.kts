plugins {
    //id("java")
    id("application")
}

group = "be.kdg.programming3"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}
application {
    mainClass.set("be.kdg.programming3.StartApplication")
}

dependencies {
    implementation("com.afrunt.randomjoke:random-joke-crawler:0.2.2")
    testImplementation(platform("org.junit:junit-bom:5.9.1"))
    testImplementation("org.junit.jupiter:junit-jupiter")
}

tasks.test {
    useJUnitPlatform()
}